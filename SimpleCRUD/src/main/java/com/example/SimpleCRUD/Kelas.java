package com.example.SimpleCRUD;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Kelas{
    @Id
    private String kelas_id;
    private String nama_kelas;

    /**
     * @return the kelas_id
     */
    public String getKelas_id() {
        return kelas_id;
    }

    /**
     * @param kelas_id the kelas_id to set
     */
    public void setKelas_id(String kelas_id) {
        this.kelas_id = kelas_id;
    }

    /**
     * @return the nama_kelas
     */
    public String getNama_kelas() {
        return nama_kelas;
    }

    /**
     * @param nama_kelas the nama_kelas to set
     */
    public void setNama_kelas(String nama_kelas) {
        this.nama_kelas = nama_kelas;
    }

    @Override
    public String toString(){
        return "kelas {" + "kelas_id='"+ kelas_id +'\''+",nama_kelas='"+nama_kelas+'\''+'}';
    }
}