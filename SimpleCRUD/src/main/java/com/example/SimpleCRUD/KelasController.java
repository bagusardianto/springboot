package com.example.SimpleCRUD;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.web.BadHttpRequest;

import org.springframework.web.bind.annotation.PutMapping;




@RestController
@RequestMapping(path = "/kelas")
public class KelasController {

    @Autowired 
    KelasRepository repo;

    @GetMapping
    public Iterable<Kelas> findAll(){
        return repo.findAll();
    }

    @GetMapping(path="/{kelas_id}")
    public Optional<Kelas> find(@PathVariable("kelas_id") String kelas_id) {
        return repo.findById(kelas_id);
    }

    @PostMapping(consumes = "application/json")
    public Kelas create(@RequestBody Kelas kelas){
        return repo.save(kelas);
    }

    @DeleteMapping(path="/{kelas_id}")
    public void delete(@PathVariable("kelas_id") String kelas_id){
        repo.deleteById(kelas_id);
    }

    @PutMapping(path="/{kelas_id}")
    public Kelas update(@PathVariable("kelas_id") String kelas_id, @RequestBody Kelas kelas) throws BadHttpRequest {
        if(repo.existsById(kelas_id)){
            kelas.setKelas_id(kelas_id);
            return repo.save(kelas);
        }else{
            throw new BadHttpRequest();
        }
    }

}