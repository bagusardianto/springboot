package com.example.SimpleCRUD;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface KelasRepository extends JpaRepository<Kelas, String>{
    
}